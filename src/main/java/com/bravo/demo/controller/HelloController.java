package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
@Controller
public class HelloController {
    @GetMapping("/greeting/{user}")//GetMapping annotation to define what url map this method is identifies with.
    @ResponseBody//defines what the response will be once the url is given
    public String helloName(@PathVariable String user){//the method called on the response body annotation
        return "Hello " + user + "!";//returning string message...
    }
//    @GetMapping("/hello")//GetMapping annotation to define what url map this method is identifies with.
//    @ResponseBody//defines what the response will be once the url is given
//    public String hello(){//the method called on the response body annotation
//        return "Hello from springBoot!";//returning string message...
//    }
//    @RequestMapping(path = "/favorite/{number}")
//    @ResponseBody
//    public String sayFavNum(@PathVariable int number){//@PathVariables identifies the variable object to the @RequestMapping url path.
//        return "Looks like your favorite number is:" + number;
//    }
}
