package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
@Controller
public class MathController {
    @GetMapping("/add/{num1}/with/{num2}")
    @ResponseBody
    public int addNumbers(@PathVariable int num1, @PathVariable int num2){
        return num1 + num2;
    }
    @GetMapping("/add/{num1}/and/{num2}")
    @ResponseBody
    public String addNumbers2(@PathVariable int num1, @PathVariable int num2){
        return num1 + " + " + num2 + " = " + addNumbers(num1, num2);
    }
    @GetMapping("/subtract/{num1}/with/{num2}")
    @ResponseBody
    public int subtractNumbers(@PathVariable int num1, @PathVariable int num2){
        return num1 - num2;
    }
    @GetMapping("/subtract/{num1}/and/{num2}")
    @ResponseBody
    public String subtractNumbers2(@PathVariable int num1, @PathVariable int num2){
        return num1 + " - " + num2 + " = " + subtractNumbers(num1, num2);
    }
    @GetMapping("/multiply/{num1}/with/{num2}")
    @ResponseBody
    public int multiplyNumbers(@PathVariable int num1, @PathVariable int num2){
        return num1 * num2;
    }
    @GetMapping("/multiply/{num1}/and/{num2}")
    @ResponseBody
    public String multiplyNumbers2(@PathVariable int num1, @PathVariable int num2){
        return num1 + " x " + num2 + " = " + multiplyNumbers(num1, num2);
    }
    @GetMapping("/divide/{num1}/with/{num2}")
    @ResponseBody
    public int divideNumbers(@PathVariable int num1, @PathVariable int num2){
        return num1 / num2;
    }
    @GetMapping("/divide/{num1}/and/{num2}")
    @ResponseBody
    public String divideNumbers2(@PathVariable int num1, @PathVariable int num2){
        return num1 + "/" + num2 + " = " + divideNumbers(num1, num2);
    }
}
