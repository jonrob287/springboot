package com.bravo.demo.controller;

import com.bravo.demo.model.Book;
import com.bravo.demo.repositories.BookRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class BookController {
    //repository instance/variable
    //Dependency Injection
    private final BookRepository bookRepository;

    //Dependency Injection
    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    //
    @GetMapping("/books")
    public String index(Model model){
        model.addAttribute("books", bookRepository.findAll());
        //return this books/index
        return "books/index"; //index = name of view
    }

    //Create a GetMapping for when we hit submit on our form
    @GetMapping("books/form")
    public String showAddBookForm(Model model){
        model.addAttribute("book", new Book());
        return "books/add-book"; // add-book = name of our view
    }

    //add a book

    //PostMapping to our /books that shows our book list
    @PostMapping("books/form")
    public String addBook(@ModelAttribute Book book){

        //save a book entry
        bookRepository.save(book);
        return "redirect:/books";
    }

    //Edit part 1
    @GetMapping("books/{id}/edit-book")
    public String viewEditBook(@PathVariable Integer id, Model model){
        model.addAttribute("book", bookRepository.findById(id));
        return "books/edit";
    }

    //Edit part 2
    @PostMapping("books/{id}/edit-book")
    public String updateBook(@PathVariable Integer id, @ModelAttribute Book book){
        bookRepository.save(book);
        return "redirect:/books";
    }


    //Delete all books
    @PostMapping("books/{id}/delete-book")
    public String deleteBook(@PathVariable Integer id){
        //delete books
        bookRepository.deleteById(id);
        return "redirect:/books";
    }


}
