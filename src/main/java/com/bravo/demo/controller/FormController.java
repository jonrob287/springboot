package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FormController {
    @GetMapping("/show-form")
    public String showInitialForm(){
        return "company-form";
    }
    @PostMapping("/process-form")
    public String showFormSubmitted(
//            @RequestParam(name="firstName")String firstName, Model firstNameModel,
//            @RequestParam(name="lastName")String lastName, Model lastNameModel,
//            @RequestParam(name="city")String city, Model cityModel,
//            @RequestParam(name="state")String state, Model stateModel,
//            @RequestParam(name="email")String email, Model emailModel

            @RequestParam(name="firstName")String firstName,
            @RequestParam(name="lastName")String lastName,
            @RequestParam(name="city")String city,
            @RequestParam(name="state")String state,
            @RequestParam(name="email")String email,
            Model infoModel){
        infoModel.addAttribute("name", "Name:" + firstName + " " + lastName);
        infoModel.addAttribute("hometown", "Hometown: " + city + " " + state);
        infoModel.addAttribute("contact", "Email: " + email);

//        firstNameModel.addAttribute("firstName", firstName);
//        lastNameModel.addAttribute("lastName", lastName);
//        cityModel.addAttribute("city", city);
//        stateModel.addAttribute("state",state);
//        emailModel.addAttribute("email", email);

        return "show-form-submit";
    }

}
