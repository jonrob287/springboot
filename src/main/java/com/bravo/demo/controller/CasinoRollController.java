package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.Random;

@Controller
public class CasinoRollController {
//    static Math.Random rand= new Math.Random();
//    public static int rollDice(){
//        int die1 =(int)(Math.random()*((5)+1));
//        return die1;
//    }

    @GetMapping("/casino")
    public String showCasinoRoll() {
        return "casino-roll";

    }

    @GetMapping("/casino/{choice}")
    public String rollChoice(@PathVariable int choice, Model model){
        model.addAttribute("choice", choice);
        int roll =(int)(Math.floor(Math.random() * 6) + 1);
        model.addAttribute("random", + roll);
        String result;
        if (choice == roll){
            result = "You Guessed Correctly!";
        } else {
            result = "Oops! Try Again...";
        }
        model.addAttribute("result", result);
        return "casino-roll-choice";
    }
}
