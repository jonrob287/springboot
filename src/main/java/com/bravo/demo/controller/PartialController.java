package com.bravo.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PartialController {
    @GetMapping ("/site")
    public String getLandingPage(){
        return "landingPage";
    }
}
