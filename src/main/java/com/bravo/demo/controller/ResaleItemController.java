package com.bravo.demo.controller;

import com.bravo.demo.model.ResaleItems;
import com.bravo.demo.repositories.ResaleItemRepo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;

@Controller
public class ResaleItemController {
    private ResaleItemRepo itemDao;

    public ResaleItemController(ResaleItemRepo itemDao){
        this.itemDao = itemDao;
    }
    @GetMapping ("/resaleItems")
    @ResponseBody
    public List<ResaleItems>getAllItems(){
        return itemDao.findAll();
    }
}
