package com.bravo.demo.controller;

import com.bravo.demo.model.User;
import com.bravo.demo.repositories.UserRepo;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class UserController {
    //These two variables will provide access to Jpa API through repo variable
//PasswordEncoder variable comes from spring security and will secure the password
    private UserRepo users;
    private PasswordEncoder passwordEncoder;
    public UserController(UserRepo users, PasswordEncoder passwordEncoder) {
        this.users = users;
        this.passwordEncoder = passwordEncoder;
    }

    //sign-up form calling Model to send to front end
    @GetMapping("/sign-up")
    public String showSignupForm(Model model){
        model.addAttribute("user", new User());
        return "users/sign-up";
    }
    //saving user
    @PostMapping("/sign-up")
    public String saveUser(@ModelAttribute User user){
        String hash = passwordEncoder.encode(user.getPassword());//assigning hash to passwordEncoder and attaching .getPassword() method onto user to get the password
        boolean debug = passwordEncoder.matches(user.getPassword(),hash);
        user.setPassword(hash);//setting the password for the user
        users.save(user);//saving user and user info
        return "redirect:/showNewEmployeeForm";//a simple redirect if credentials are good
    }









//    @Autowired
//    UserRepository userRepo;
//
//    @RequestMapping("/users")
//    public String home(Model model){
//        model.addAttribute("users", userRepo.findAll());
//        return "users";
//    }

}
