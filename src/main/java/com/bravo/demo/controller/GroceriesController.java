package com.bravo.demo.controller;
        import com.bravo.demo.model.Grocery;
        import com.bravo.demo.services.GroceriesService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.data.domain.Page;
        import org.springframework.stereotype.Controller;
        import org.springframework.ui.Model;
        import org.springframework.web.bind.annotation.*;
        import java.util.List;
@Controller
public class GroceriesController {
    @Autowired
    private GroceriesService groceriesService;
    @GetMapping("/groceries")
    public String viewHomePage(Model model) {
        return findPaginated(1, "name", "asc", model);
    }
    @GetMapping("/showNewGroceryForm")
    public String showNewGroceryForm(Model model) {
        Grocery grocery = new Grocery();
        model.addAttribute("grocery", grocery);
        return "/groceries/add-grocery";
    }
    @PostMapping("/saveGrocery")
    public String saveGrocery(@ModelAttribute("grocery") Grocery grocery) {
        groceriesService.saveGrocery(grocery);
        return "redirect:/groceries";
    }
    @GetMapping("/showGroceryFormUpdate/{id}")
    public String showGroceryFormUpdate(@PathVariable( value = "id") long id, Model model) {
        Grocery grocery = groceriesService.getGroceryById(id);
        model.addAttribute("grocery", grocery);
        return "/groceries/update_groceries";
    }
    @GetMapping("/deleteGrocery/{id}")
    public String deleteGrocery(@PathVariable (value = "id") long id) {
        this.groceriesService.deleteGroceryById(id);
        return "redirect:/groceries";
    }
    @GetMapping("/groceriesPages/{pageNo}")
    public String findPaginated(@PathVariable (value = "pageNo") int pageNo,
                                @RequestParam("sortField") String sortField,
                                @RequestParam("sortDir") String sortDir,
                                Model model) {
        int pageSize = 5;
        Page<Grocery> page = groceriesService.findPaginated(pageNo, pageSize, sortField, sortDir);
        List<Grocery> listGroceries = page.getContent();
        model.addAttribute("currentPage", pageNo);
        model.addAttribute("totalPages", page.getTotalPages());
        model.addAttribute("totalItems", page.getTotalElements());
        model.addAttribute("sortField", sortField);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
        model.addAttribute("listGroceries", listGroceries);
        return "/groceries/index";
    }
}
