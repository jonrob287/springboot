package com.bravo.demo.repositories;

import com.bravo.demo.model.ResaleItems;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResaleItemRepo extends JpaRepository<ResaleItems, Long> {
    ResaleItems findByTitle(String Items);
}
